import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { Left, Body, H3, Card, Right, CardItem, Icon, Button, Text, Thumbnail, Toast } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

import Portada from './Portada';

class LibreroCard extends Component {
  constructor(props) {
    super(props);
    this.removeCard = this._removeCard.bind(this);
    this.libreroCard = props.libreroCard;
  }

  async _removeCard(idElemento) {
    let favoritosList = await AsyncStorage.getItem('localFavoritos');
    let localFavoritos = await JSON.parse(favoritosList) || [];
    let index = localFavoritos.findIndex((e) => e.id === idElemento)
    localFavoritos.splice(index, 1);
    AsyncStorage.setItem('localFavoritos', JSON.stringify(localFavoritos));
    Toast.show({text: 'Libro eliminado', position: 'bottom', duration:1500});
  }

  render() {
    return (
      <Card style={{ paddingLeft: 0, width: '96%', marginLeft: '2%' }}>
        <Grid style={{ paddingLeft: 0 }}>
          <Row style={{}} >
            <Col style={{ padding: 0, margin: 0, width: '20%', backgroundColor: '#eeeeee' }}>

              {/* Portada del libro */}
              <Portada style={{ padding: 0, margin: 0, width: '100%', backgroundColor: 'red' }}
                portada={(this.libreroCard.portada === '' ? 'empty' : this.libreroCard.portada)}
                isbn={(!this.libreroCard.isbn || this.libreroCard.isbn === '[sin ISBN]' || this.libreroCard.isbn === null || this.libreroCard.isbn === 'NULL' ? '0000' : this.libreroCard.isbn.replace(/-/g, ''))} />

            </Col>
            <Col style={{ width: '80%', padding: 10 }} >

              {/* Titulo del libro */}
              <H3 numberOfLines={2} style={{ color: '#212121', padding: 4, fontFamily: 'sans-serif-light' }}>
                {this.libreroCard.titulo}
              </H3>

              <Text numberOfLines={1} style={{ color: '#9e9e9e', paddingBottom: 4 }}>{this.libreroCard.autor}</Text>
              <Text numberOfLines={1} style={{ color: '#9e9e9e', paddingBottom: 4 }}>{this.libreroCard.clasificacion}</Text>
              <Row>
                <Col>

                  {/* Botón para ver detalles */}
                  <Button transparent onPress={() => this.props.navigation.navigate('Details', { id: this.libreroCard.id })} >
                    <Text style={{ paddingLeft: 4, paddingRight: 4 }}>Ver detalles</Text>
                  </Button>

                </Col>
                <Right>
                  <Col>

                    {/* Botón para eliminar elemento */}
                    <Button transparent onPress={() => this.removeCard(this.libreroCard.id)}>
                      <Icon style={{ color: '#bdbdbd' }} name='ios-trash' />
                    </Button>

                  </Col>
                </Right>
              </Row>
            </Col>
          </Row>
        </Grid>
      </Card>
    );
  }
}

export default LibreroCard;
