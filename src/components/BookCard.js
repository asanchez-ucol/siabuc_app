import React, { Component } from 'react';
import { StyleSheet, Dimensions, TouchableNativeFeedback } from 'react-native';
import { Left, Body, Card, Right, CardItem, Icon, Button, Text, H3, Thumbnail } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

import Portada from './Portada';

class BookCard extends Component {
  constructor(props) {
    super(props);
    this.bookCard = props.bookCard;
  }

  render() {
    if (this.bookCard.key) {
      return (
        <TouchableNativeFeedback style={{ flex: 1 }} onPress={() => this.props.navigation.navigate('Details', { id: this.bookCard.key })}>

          <Card style={s.cardContainer} >
            <Grid>
              <Row>

                <Col style={s.coverSection}>
                  <Portada portada={(this.bookCard.portada === '' ? 'empty' : this.bookCard.portada)} isbn={(!this.bookCard.isbn || this.bookCard.isbn === '[sin ISBN]' || this.bookCard.isbn === null || this.bookCard.isbn === 'NULL' ? '0000' : this.bookCard.isbn.replace(/-/g, ''))} />
                </Col>

                <Col style={{ width: '80%', paddingVertical: 18, paddingHorizontal: 14 }}>

                  <Text numberOfLines={2} style={s.title}>
                    {this.bookCard.titulo}
                  </Text>

                  <Text numberOfLines={1} style={s.autor}>{this.bookCard.autor}</Text>
                  <Text numberOfLines={1} style={s.clasificacion}>{this.bookCard.clasificacion}</Text>

                </Col>
              </Row>
            </Grid>

          </Card>
        </TouchableNativeFeedback>
      );
    }
  }
}

export default BookCard;


const screen = Dimensions.get('screen');

const s = StyleSheet.create({

  cardContainer: {
    paddingLeft: 0, width: '96%', marginLeft: '2%', marginTop: 8
  },

  coverSection: {
    width: '20%',
    padding: 4
  },

  title: {
    fontSize: 20,
    color: '#424242',
    fontFamily: 'sans-serif-light'
  },

  autor: {
    color: '#9e9e9e', paddingTop: 6
  },

  clasificacion: {
    color: '#9e9e9e', paddingBottom: 2
  },

  mt1: {
    marginTop: '6%',
  },

  mt2: {
    marginTop: '18%',
  }

});

