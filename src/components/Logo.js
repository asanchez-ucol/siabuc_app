import React, { Component } from 'react';
import { Thumbnail } from 'native-base';

class Logo extends Component {
  constructor(props) {
    super(props);
    this.state = { ruta: "../../assets/images/logo.png" };
  }

  render() {

    return (
      <Thumbnail square small source={require("../../assets/images/logo.png")} />
    );
  }
}

export default Logo;
