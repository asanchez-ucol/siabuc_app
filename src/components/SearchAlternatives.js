import React, { Component } from "react";
import { StyleSheet, View, Dimensions } from 'react-native';
import { Text, Thumbnail, H3 } from 'native-base';

export class SearchNone extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isbn: this.props.isbn,
      portada: this.props.portada,
      origen: 'default'
    };
  }

  render() {
    return (
      /* Este View corresponde a lo que se mostrará cuando se realice la primera búsqueda  */

      <View style={[s.center]}>
        <View style={{ flex: 1 }} />
        <View style={{ flex: 6 }}>
          <Thumbnail resizeMode="contain" square style={{}} style={[s.image, s.mt2]} source={require('../../assets/images/reading.png')} />
        </View>
        <H3 style={s.info}>Hola, bienvenido</H3>
        <View style={{ flex: 2 }}>
          <Text style={[s.info, s.pt]}>¿Buscas algún libro en  particular? Empieza poniendo un título en la barra de búsqueda, aquí arriba </Text>
        </View>
        <View style={{ flex: 1 }} />
      </View >
    );
  }
}

export class SearchEmpty extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isbn: this.props.isbn,
      portada: this.props.portada,
      origen: 'default'
    };
  }

  render() {
    return (
      /* Este View corresponde a lo que se mostrará cuando no se encuentren resultados */

      <View style={[s.center]}>
        <View style={{ flex: 1 }} />
        <View style={{ flex: 6 }}>
          <Thumbnail resizeMode="contain" square style={[s.notFoundimage, s.mt1]} source={require('../../assets/images/notFound.png')} />
        </View>
        <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}  >
        </View>
        <H3 style={s.info}>No se encontraron resultados</H3>
        <View style={{ flex: 2 }}>
          <Text style={[s.info, s.pt]}>Ups, parece que no hay libros que coincidan con esta búsqueda. Intenta usar otras palabras u otro parámetro de búsqueda</Text>
        </View>
        <View style={{ flex: 1 }} />
      </View >
    );
  }
}

const screen = Dimensions.get('screen');
const s = StyleSheet.create({
  mt1: {
    marginTop: '6%',
  },
  mt2: {
    marginTop: '18%',
  },
  image: {
    flex: 1,
    width: screen.width * 0.48,
    height: screen.height * 0.38,
    alignSelf: 'stretch',
  },
  notFoundimage: {
    flex: 1,
    width: screen.width * 0.34,
    height: screen.height * 0.34,
  },
  info: {
    marginLeft: screen.width * 0.14,
    marginRight: screen.width * 0.14,
    color: '#424242',
    textAlign: 'center'
  },
  pt: {
    marginTop: 15,
    marginBottom: 8
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  }
});
