import React, { Component } from "react";
import { Image } from 'react-native';

const notAvailable = require('../../assets/images/sin-portada.png');
class Portada extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isbn: this.props.isbn,
      portada: this.props.portada,
      origen: 'default'
    };
  }

  componentWillMount() {
    if (this.state.portada === 'empty') {
      this.setState({ origen: 'ol' }, () => this.getPortada());
    } else {
      this.getPortada();
    }
  }

  getPortada() {
    let nvaPortada = '';
    switch (this.state.origen) {
      case 'default':
        nvaPortada = this.state.portada;
        break;
      case 'ol':
        nvaPortada = 'http://covers.openlibrary.org/b/isbn/' + this.state.isbn + '-M.jpg?default=false';
        break;
      case 'amazon':
        nvaPortada = 'http://images.amazon.com/images/P/' + this.state.isbn + '.01.MZZZZZZZ.jpg';
        break;
      case 'google':
        nvaPortada = 'https://www.googleapis.com/books/v1/volumes?q=isbn:' + this.state.isbn;
        break;
      case 'nd':
        nvaPortada = '../../assets/images/sin-portada.png';
        break;
    }

    if (this.state.origen !== 'google') {
      this.setState({ portada: nvaPortada });
    } else {
      fetch(nvaPortada, {
        method: 'get'
      })
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          if (data.totalItems > 0) {
            this.setState({ portada: (data.items[0].volumeInfo.imageLinks.thumbnail ? data.items[0].volumeInfo.imageLinks.thumbnail : null) });
          } else {
            this.errorPortada();
          }
        });
    }
  }

  errorPortada() {
    let nvoOrigen = '';
    switch (this.state.origen) {
      case 'default':
        nvoOrigen = 'ol';
        break;
      case 'ol':
        nvoOrigen = 'amazon';
        break;
      case 'amazon':
        nvoOrigen = 'google';
        break;
      case 'google':
        nvoOrigen = 'nd';
        break;
    }
    this.setState({ origen: nvoOrigen }, () => this.getPortada());
  }

  cargaPortada() {
    if (this.state.origen === 'amazon') {
      Image.getSize(this.state.portada, (width, height) => {
        if (width === 1 && height === 1) {
          this.setState({ origen: 'google' }, () => this.getPortada());
        }
      });
    }
  }

  render() {
    const resizeMode = 'cover';
    let image = null;
    if (this.state.origen === 'nd') {
      image = notAvailable;
    } else {
      image = { uri: this.state.portada };
    }
    return (
      <Image
        source={image}
        onError={() => this.errorPortada()}
        onLoad={() => this.cargaPortada()}
        ref={c => this.portada = c}
        {...this.props}
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: '#eeeeee',
          resizeMode,
        }}
      />
    );
  }
}

export default Portada;
