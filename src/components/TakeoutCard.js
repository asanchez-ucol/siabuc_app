import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { Left, Body, H3, Card, Right, CardItem, Icon, Button, Text, Thumbnail, Toast } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

import Portada from './Portada';

class TakeoutCard extends Component {
  constructor(props) {
    super(props);
    this.removeCard = this._removeCard.bind(this);
    this.takeoutCard = props.takeoutCard;
  }

  async _removeCard(idElemento) {
    let takeoutList = await AsyncStorage.getItem('localTakeout');
    let localTakeout = await JSON.parse(takeoutList) || [];
    let index = localTakeout.findIndex((e) => e.id === idElemento)
    localTakeout.splice(index, 1);
    AsyncStorage.setItem('localTakeout', JSON.stringify(localTakeout));
    this.props.getTakeoutList();
    Toast.show({text: 'Libro eliminado', position: 'bottom', duration:1500});
  }

  render() {
    return (

      <Card style={{ paddingLeft: 0, width: '96%', marginLeft: '2%' }}>

        <Grid style={{ paddingLeft: 0 }}>
          <Row style={{}} >
            <Col style={{ padding: 0, margin: 0, width: '20%', backgroundColor: '#eeeeee' }}>
              <Portada style={{ padding: 0, margin: 0, width: '100%', backgroundColor: 'red' }}
                portada={(this.takeoutCard.portada === '' ? 'empty' : this.takeoutCard.portada)}
                isbn={(!this.takeoutCard.isbn || this.takeoutCard.isbn === '[sin ISBN]' || this.takeoutCard.isbn === null || this.takeoutCard.isbn === 'NULL' ? '0000' : this.takeoutCard.isbn.replace(/-/g, ''))}
              />
            </Col>
            <Col style={{ width: '80%', padding: 10 }} >
              <H3 numberOfLines={2} style={{ color: '#212121', padding: 4, fontFamily: 'sans-serif-light' }}>
                {this.takeoutCard.titulo}
              </H3>
              <Text numberOfLines={1} style={{ color: '#9e9e9e', paddingBottom: 4 }}>{this.takeoutCard.autor}</Text>
              <Text numberOfLines={1} style={{ color: '#9e9e9e', paddingBottom: 4 }}>{this.takeoutCard.clasificacion}</Text>
              <Row>
                <Col>

                  {/* Botón para ver detalles */}
                  <Button transparent onPress={() => this.props.navigation.navigate('Details', { id: this.takeoutCard.id })} >
                    <Text style={{ paddingLeft: 4, paddingRight: 4 }}>Ver detalles</Text>
                  </Button>

                </Col>
                <Right>
                  <Col>

                    {/* Botón para eliminar elemento */}
                    <Button transparent onPress={() => this.removeCard(this.takeoutCard.id)}>
                      <Icon style={{ color: '#bdbdbd' }} name='ios-trash' />
                    </Button>

                  </Col>
                </Right>
              </Row>
            </Col>
          </Row>
        </Grid>
      </Card>
    );
  }
}

export default TakeoutCard;
