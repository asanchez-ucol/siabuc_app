import React, { Component } from 'react';
import { StyleSheet, View, TouchableHighlight, Animated } from 'react-native';
import { StyleProvider, getTheme, Body, Button, Text, Icon } from 'native-base';


class Panel extends Component {
  constructor(props) {
    super(props);
    this.icons = {
      'up': 'chevron-up',
      'down': 'chevron-down'
    };
    this.state = {
      title: this.props.title,
      iconName: this.props.name,
      iconFamily: this.props.family,
      expanded: true,
      maxHeight: null,
      minHeight: null,
      animation: new Animated.Value(),
      firstRender: false
    };
  }

  toggle() {
   let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight;
   let finalValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight + this.state.minHeight;
   this.setState({
     expanded : !this.state.expanded
   });
   this.state.animation.setValue(initialValue);
   Animated.spring(
     this.state.animation,
     {
       toValue: finalValue
     }
   ).start();
 }

 afterFirstrender() {
   if (!this.state.firstRender) {
     this.toggle();
     this.setState({ firstRender: true });
   }
 }

  _setMaxHeight(event) {
    this.setState({
      maxHeight: event.nativeEvent.layout.height
    });
  }

  _setMinHeight(event) {
    this.setState({
      minHeight: event.nativeEvent.layout.height + (event.nativeEvent.layout.height * 0.2)
    }, () => this.afterFirstrender());
  }

  render() {
    let icon = this.icons['down'];
    if(this.state.expanded) {
      icon = this.icons['up'];
    }
    return (
      <Animated.View style={[styles.container,{height: this.state.animation}]}>
        <View style={styles.container} >
          <View style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>
            <StyleProvider style={getTheme({ iconFamily: this.state.iconFamily })}>
              <Icon name={this.state.iconName} />
            </StyleProvider>
            <Text style={styles.title}>{this.state.title}</Text>
            <Button onPress={this.toggle.bind(this)} small transparent info>
              <StyleProvider style={getTheme({ iconFamily: 'FontAwesome' })}>
                <Icon name={icon} />
              </StyleProvider>
            </Button>
          </View>
          <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
            {this.props.children}
          </View>
        </View>
      </Animated.View>
    );
  }
}

export default Panel;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    margin: 10,
    overflow: 'hidden'
  },
  titleContainer: {
    flexDirection: 'row'
  },
  title: {
    flex: 1,
    padding: 10,
    color: '#2a2f43',
    fontWeight: 'bold'
  },
  body: {
    padding: 10,
    paddingTop: 0
  }
});
