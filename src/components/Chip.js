import React, { Component } from 'react';
import { Container, Header, Content, Badge, Text, Icon } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';




class Chip extends Component {
  render() {

    return (
      <Text style={{
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        marginRight: 10,
        marginBottom: 10,
        backgroundColor: '#E4E4E4',
        color: '#5B5B5B'
      }}>{this.props.content}</Text>

    );
  }
}

export default Chip;
