import React, { Component } from "react";
import { StyleProvider, getTheme, Icon } from "native-base";

class IconWrapper extends Component {
    constructor(props) {
        super(props);
        this.familyName = this.props.familyName;
        this.iconName = this.props.iconName;
    }

    render() {
        return (
            <StyleProvider style={getTheme({ iconFamily: this.familyName })}>
                <Icon name={this.iconName} {...this.props}/>
            </StyleProvider>
        );
    }
}

export default IconWrapper;