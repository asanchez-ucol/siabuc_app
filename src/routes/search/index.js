/* 
  Component Name: SearchScreen
  Developer Name: Roberto Martínez
  Date Modified: 2018/03/14
  Description: Este componente contiene toda la lógica de renderizado de la pantalla de búsqueda
    y aquí se montan los anuncios de AdMob
  Props: Navigation
*/
import React, { Component } from "react";
import { View, ListView, TextInput } from "react-native";
import {
  Header,
  Container,
  Footer,
  Item,
  Icon,
  Content,
  List,
  ListItem,
  Card,
  CardItem,
  Spinner,
  Button,
  Text,
  Thumbnail
} from 'native-base';

import BookCard from "../../components/BookCard";
import Logo from "../../components/Logo";
import IconWrapper from "../../components/IconWrapper";
import { SearchNone, SearchEmpty } from "../../components/SearchAlternatives";

class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchString: null,
      dataset: null,
      _data: null,
      dataOffset: 0,
      resultLimit: 15,
      isLoading: false,
      isLoadingMore: false,
      firstSearch: false,
      resultados: 0,
      showRNumber: false,
      baseEndPoint: "http://siabuc.ucol.mx:3000/api/fichas/busqueda/",
      filterActive: false,
      filterString: null
    };
    this.filtersConfigured = this.filtersConfigured.bind(this);
    siabucHelpers = require("../../helpers/helpers.js");
  }

  // <Business Logic>

  getCount() {
    return fetch(this.state.baseEndPoint + `${this.state.searchString}?count` + (this.state.filterString ? this.state.filterString : ''))
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          resultados: responseJson[0].titulos,
          nResultados: true
        });
      })
      .catch((error) => {
      });
  }

  fetchMore() {
    const targetUri = this.state.baseEndPoint
      + `${this.state.searchString}?after=${this.state.dataOffset}&limit=${this.state.resultLimit}`
      + (this.state.filterString ? this.state.filterString : '');
    siabucHelpers.fetchData((responseJson) => {
      const data = this.state._data.concat(responseJson);
      this.setState({
        dataset: this.state.dataset.cloneWithRows(data),
        isLoadingMore: false,
        _data: data
      });
    }, targetUri);
  }

  dataNewSearch() {
    const targetUri = this.state.baseEndPoint + `${this.state.searchString}?after=${this.state.dataOffset}&limit=${this.state.resultLimit}` + (this.state.filterString ? this.state.filterString : '');
    siabucHelpers.fetchData((responseJson) => {
      let dSource = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      });
      const data = responseJson;
      this.setState({
        dataset: dSource.cloneWithRows(data),
        _data: data,
        isLoading: false
      });
    }, targetUri);
  }

  newSearchEvent() {
    if (this.state.searchString) {
      this.setState({ dataOffset: 0 }, () => {
        this.getCount();
        this.dataNewSearch();
      });
    }
  }

  filtersConfigured(filterString) {
    this.setState({
      filterString,
      filterActive: (filterString ? true : false)
    }, () => this.newSearchEvent());
  }

  // </Business Logic>

  // <Render Logic>

  renderItem() {
    if (!this.state.firstSearch) {
      return <SearchNone />;
    } else if (this.state.isLoading) {
      return <Spinner />;
    } else if (this.state.dataset._dataBlob.s1 && this.state.dataset._dataBlob.s1.length > 0) {
      return this.state.dataset._dataBlob.s1.map((bookCard) => {
        if (bookCard.admob) {
          return null;
        }
        return <BookCard navigation={this.props.navigation} bookCard={bookCard} key={bookCard.key} />;
      });
    }
    return <SearchEmpty />;
  }

  renderFooter() {
    if (this.state.isLoadingMore) {
      return <Spinner />;
    }
  }

  renderCount() {
    if (this.state.nResultados && this.state.firstSearch) {
      return (
        <Text style={{ textAlign: 'center', margin: 10 }}>Se obtuvieron {this.state.resultados} resultados</Text>
      );
    }
  }

  renderBanner() {
    if (this.state.firstSearch) {
      return siabucHelpers.getBanner();
    }
    return null;
  }

  renderIcon() {
    if (this.state.filterActive) {
      return <IconWrapper primary familyName="Ionicons" iconName="md-options" />
    }
    return <IconWrapper style={{ color: 'black' }} familyName="Ionicons" iconName="md-options" />
  }

  // </Render Logic>

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container >
        <Header searchBar rounded>
          <Item style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Logo />
          </Item>
          <Item style={{ flex: 9 }}>
            <Content scrollEnabled={false}>
              <View style={{
                flexDirection: 'row',
                width: window.width,
                alignItems: 'center',
                justifyContent: 'center',

              }}>
                <View style={{ flex: 1 }}>
                  <TextInput
                    returnKeyType={"search"}
                    onSubmitEditing={() => this.setState({ firstSearch: true, isLoading: true }, () => this.newSearchEvent())}
                    placeholder="¿Qué buscas?"
                    onChangeText={text => this.setState({ searchString: text })}
                    underlineColorAndroid={"transparent"}
                    style={{ paddingTop: 0, paddingBottom: 0, paddingLeft: 20 }} />
                </View>
                <View>
                  <Button onPress={() => this.props.navigation.navigate('Filters', { filtersConfigured: this.filtersConfigured })} small transparent info>
                    {this.renderIcon()}
                  </Button>
                </View>
              </View>
            </Content>
          </Item>
        </Header>
        <Content style={{ flex: 1, backgroundColor: 'white' }}
          scrollEventThrottle={300}
          onScroll={(e) => {
            let paddingToBottom = 10;
            paddingToBottom += e.nativeEvent.layoutMeasurement.height;
            if (e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom) {
              const dos = this.state.dataOffset + this.state.resultLimit;
              this.setState({
                isLoadingMore: true,
                dataOffset: dos
              }, () => {
                if (this.state.searchString) {
                  this.fetchMore();
                }
              });
            }
          }}
          removeClippedSubviews={true}>
          {this.renderCount()}
          {this.renderItem()}
          {this.renderFooter()}
        </Content>
        {this.renderBanner()}
      </Container>
    );
  }
}

export default SearchScreen;
