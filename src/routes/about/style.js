var screen = Dimensions.get('screen');
import { Dimensions } from 'react-native';

export default {

    content: {
        width: '90%',
        marginLeft: '5%'
    },

    title: {
        color: "#424242"
    },

    subtitle: {
        color: "#757575",
    },

    bordered: {
        borderRadius: 0,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
    },

    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    avatar: {
        width: screen.width * 0.33,
        height: screen.height * 0.2,
        borderRadius: 200
    },

    softButton: {
        backgroundColor: null,
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        elevation: 0,
    },

    softIcon: {
        color: '#bdbdbd',
        fontSize: 16,
    },

    thin: {
        fontFamily: 'sans-serif-light'
    },

    logo: {
        width: screen.width * 0.28, height: screen.height * 0.093
    },

    cellphone: {
        width: screen.width * 0.6, height: screen.height * 0.34
    },

    scrollView: {},


    //Margenes
    mt1: {
        marginTop: '4%'
    },

    mt2: {
        marginTop: '8%'
    },

    mb1: {
        marginBottom: '4%'
    },

    mb2: {
        marginBottom: '8%'
    },
    ml2: {
        marginLeft: 18
    },

    ml1: {
        marginLeft: 12,
    },

    mr2: {
        marginRight: 18
    },


    detailsContainer: {
        flex: 2,
        flexDirection: 'column',
        alignItems: 'flex-start',
        marginLeft: 25,
        marginRight: 25,
        marginTop: 20,
    },

    data: {
        color: "#212121"
    },

    cardData: {
        color: "#616161",
        marginLeft: 10
    },
    //container style wrapper for scrollview
    footerWrapper: {
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection: 'row',
    },
    //non-container style wrapper for scrollview
    footerWrapperNC: {
        flexDirection: 'column',
    },

    sectionTitle: {
        marginTop: 30,
        marginBottom: 10,
        color: '#424242'
    }
    ,
    autor: {

        marginTop: 10,
        marginBottom: 10,

        color: "#757575",
    },
    button: {
        marginTop: 20,
        paddingLeft: 5,
        paddingRight: 5
    },
    buttonText: {
        fontSize: 18,
        color: "white"
    },
    imgContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: "grey",
    },
    disp: {
        fontSize: 20,
        color: "black",
        marginTop: 30
    },
    biblioteca: {
        marginTop: 5,
        fontSize: 18
    },
    agregarLibrero: {
        justifyContent: 'center',
        marginTop: 15
    },
    img: {
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center'
    }
};