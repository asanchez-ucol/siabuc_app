/* Imports de React y Native Base */
import React, { Component } from "react";
import { View, AppRegistry, StyleSheet, TouchableNativeFeedback, Image, ScrollView } from 'react-native';
import { Container, Header, Left, Button, Icon, Title, Body, Right, Content, Text, Footer, FooterTab, H1, H2, H3, Card, CardItem, Thumbnail } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

/* Import de estilos */
import s from './style.js';

class About extends Component {
    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Acerca de...</Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                    <Grid style={{ flex: 1 }}>
                        <Row style={{ flex: 1 }}>
                            <Col style={{ flex: 1 }}>
                                <H1 style={[s.content, s.title, s.thin, s.mt2]}>La aplicación</H1>
                                <Text style={[s.content, s.subtitle, s.thin, s.mb1]}>¿Qué hay de nuevo en la app de SIABUC, qué propósito cumple y hacia quién va dirigida?.</Text>

                                <View style={[s.center, s.mt1, s.mb1]}>
                                    <Thumbnail resizeMode="contain" style={[s.cellphone]} square source={require('../../../assets/images/two-phones.png')} />
                                </View>

                                <Text style={[s.content, s.title, s.thin]}>La aplicación de SIABUC fue creada con la finalidad de cubrir una gran necesidad. Desde profesionistas experimentados hasta estudiantes con pocos hábitos lectores; todo aquel que así lo necesite debe tener acceso a la lectura. </Text>

                                <H1 style={[s.content, s.title, s.thin, s.mt2]}>El equipo</H1>
                                <Text style={[s.content, s.subtitle, s.thin]}>Las trabajadoras almas que hicieron esto posible.</Text>

                                <ScrollView horizontal style={[s.scrollView]}>

                                    {/* Tarjeta Alex */}
                                    <Card style={{ flex: 1 }} style={[s.ml2, s.mt1]}>
                                        <CardItem style={[s.bordered]}>
                                            <Left>
                                                <Body>
                                                    <Text style={[s.title]}>Alejandro Sánchez</Text>
                                                    <Text note>CEO</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Image style={[s.avatar]} source={{ uri: 'https://www.tribute.ca/news/wp-content/uploads/2014/12/gandalf1-650x487.jpg' }} />
                                            </Body>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Row>

                                                    <Button icon rounded style={s.softButton}>
                                                        <Icon style={s.softIcon} name="mail" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-linkedin" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-twitter" />
                                                    </Button>

                                                </Row>
                                            </Body>
                                        </CardItem>
                                    </Card>

                                    {/* Tarjeta Ceballos */}
                                    <Card style={{ flex: 1 }} style={[s.ml1, s.mt1]}>
                                        <CardItem style={[s.bordered]}>
                                            <Left>
                                                <Body>
                                                    <Text style={[s.title]}>Fernando Ceballos</Text>
                                                    <Text note>Programador</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Image style={[s.avatar]} source={{ uri: 'https://myhero.com/content/images/thumbs/0035521_frodo-baggins_400.jpeg' }} />
                                            </Body>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Row>

                                                    <Button icon rounded style={s.softButton}>
                                                        <Icon style={s.softIcon} name="mail" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-linkedin" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-twitter" />
                                                    </Button>

                                                </Row>
                                            </Body>
                                        </CardItem>
                                    </Card>

                                    {/* Tarjeta Roberto */}
                                    <Card style={{ flex: 0 }} style={[s.ml1, s.mt1]}>
                                        <CardItem style={[s.bordered]}>
                                            <Left>
                                                <Body>
                                                    <Text style={[s.title]}>Roberto Martínez</Text>
                                                    <Text note>Programador</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Image style={[s.avatar]} source={{ uri: 'http://www.thehobbitblog.com/character-gallery/thcc_balin_01.jpg' }} />
                                            </Body>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Row>

                                                    <Button icon rounded style={s.softButton}>
                                                        <Icon style={s.softIcon} name="mail" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-linkedin" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-twitter" />
                                                    </Button>

                                                </Row>
                                            </Body>
                                        </CardItem>
                                    </Card>

                                    {/* Tarjeta Gustavo */}
                                    <Card style={{ flex: 0 }} style={[s.ml1, s.mt1]}>
                                        <CardItem style={[s.bordered]}>
                                            <Left>
                                                <Body>
                                                    <Text style={[s.title]}>Gustavo Arias</Text>
                                                    <Text note>Programador</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Image style={[s.avatar]} source={{ uri: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhIWFRUXFRUXFhYWFxUWFRUVFRUXFxUVFRcYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0lHSUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAACAwABBAUGB//EADoQAAEDAgQDBgQGAQMFAQAAAAEAAhEDIQQSMUEFUWEGEyJxgZFCobHBMlJi0eHwchQj8SQzY4KiB//EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACIRAAICAgMAAgMBAAAAAAAAAAABAhEDIRIxQSJREzJhgf/aAAwDAQACEQMRAD8A+U4cZajTruB1XV4ri2mGh2Z1jpFgsXd5RnJ6/suTTrxUzOmJusnGzVOjqjEwH9W/MaLlvxpOq0V8Q2IF55bLHHknjVIU3bHnHENAa5wvMbSt3esqtaKgvGuhBXLAC0ZSIghW0QjHWYWuLTsVMy21GioP1jTqsDgQYIgpoBjEx2izgowUAGEygJScypzkAbaBvb5puJe3L5rm5yqc4n0S47K5aCc2J3SwmExCB4GxTJI5CrhRMAUQI5K2notGEwL6jg1gknRJgZQVIXsG9hawZnqOY2xMSSQBHS+osulh+yNOm9raj3nOBmAaAATcBxMx8JWTzwRqsMmfPsqdQHVe+4z2JZAfReBPwvN7Wkc143iHDnUJFQSdBGnmnDLGfTFLHKPZnJAPPySSbqgratOjPssNRvCovhAJKAYSIBU1vRObQ3KLCgMp5+yMMVNiYCZdIC6bGXzQI57+SVUqA2YPZW6nNiiZDbaJUOy20zuVEJxbVEBodxKtmhjT4RvzP7LH0ISnNcFeYoodlPEaIQ9E9pJhWKcaqiSqeq1vpeXn5pLRopUpEX18khmg02sMg+p+yTjKgfcajXr1QPeT8JUbTIBOUhCBsRCtpRlqEtVCLAUIVCysEIAigCsKTZICSCb6KntA0MoQFcIApSFZUQBqp07L13Zjh7QBUeHSRDR+HQ3JJ1/lcns5gTWqNaNhmcfytESfmLdV7yphxEmQcpyDLGg/ESBc9AuXNkrR04YXsKhxsvrMZlcAG+Lw6xeQ4nSPWxScZx7MIZmdPhiJm9yVxMbiSC2mXGxIeGkAASY9LaJgxgcGsojKBGZxAzOJ2FvmVz8aOhSs6AxVmis05Hk7mGGY02ERsvP48ASQc9ImCDdwBBI84/hd3HOy0yC2PDbff+V4zEVYE7t22uTFvQK8cbdiy6RycXSyvLQZANj02SYW/ECWgnU3HlylZQ1d0Xo4JKmA1qMBWGptJgm+iZJQq7ZZ6q/ERFh9U175kBp9bJb6oA5lT2VtFsYAre8DUwslTEk9EprS4wLlOhGh+K/L7rNUqE6lNo4RznZWwT5iB5lbGYKi0xUrXGoaCR5JckhpNnLUXSFTDD4HnrOqiOX8Dj/RYcNCITO5bG6SasjRNoYkbjbmgB7aOYRuPfolYqAcqz1MVfwS0ed0LOZumkDYcJrHta0mPF7pAchc5FANZXPNXXxBLYSMyqU6Cww2yAhW1FCZIMIQEx3JCgZHNjRArcoEAWFChKIJAUpKItRUqBdppzQB6fsXWh852hxDmhou5wMWjYW+S91xDE1DT/7rpjNGou6PexXheytNtOpe9jfe4vHKei9Bgse0nu3uyuJJpvBEi4JYQbDkJtbbfjyK5nfglUaZwK7v9w8xObkTOgXqeEYGkWh4s4CSILrxb0sULOE569NpGpDiPIHNPlb3XdxPCRTEM0iD5CyV66KjDZ5kYx2Ke6mCA5oJGYFot914/iGIDXuaAJFiY3GuvVfSOD8MpEue2LOylxjXkDK8P22wrG4mpkdYu0iCDGn39VWOuXQsyahbZx6ji4AkDpAhJydF0C0NHJIfUE635f3RdKOBiWUkdR4aJKRWxHosriqEHVrk9AkyiIQwgCiotGHwT3nwt9dAunh+Au1e5oG91LnFDUWzitB2Vik7kV3ahoMt3gP+IlY8Ti6JByh5OxmAkpt+FOKXpzFEwvHJRWQEHeiNtwbDzW3uAROn0WbEU4shAZcl1oLLIxQAjxDSfJR9M6/RMBBCApjlQamAuFYRZUWVAgEwaK4Q1GJABKNoQJgbCGMXUahTahkqg0JWFCyFAmOIWjCYX4nackrHRWFobu9AtjQjAutWEwbqmgtMT13hRJ/ZtCDbpHS7OlskmZE3EaERuPNdfhWApvxIDiCAQTE72aPkfZefxeFewto0XE1KhDTG0rt8Lf8A6TGAOPgcxrb2/CAPqSsHTfZvTjpn0bDYVjXZmi+5NzA2nl0WTjdUNYXQb2gameShx7WkmVw8f2qowc1N8X/L7iXJ14XbXZwO0D3U6ctZVYYs9hZAvN4dI8140uLiXueSRclxLnE9SdSt/HeK0qk92agj4XW85gkLhZ5W0I0jmzTvofUrSblLe7l/KFpA1uoXLQ5hZaqhGQn0WN3J8km9DSEtpKOoLtcEwbatemxxytc8A3gxuAYME6AxqQtfabhPc5SGlmbNmZmDxSOY5G94Cc0symbXmy53l+VGqg2rPKukaE+6WSeadWCQV0IyZCqhWBKfRotiXPy9IJJTbAzqLQRT5vPoFEWFGqlVd+EaFSuTYE3Asiay0hUSA/xDMARImCQIkA7SLSmgYvUTqUTXEfsV79/YehiqIxHDahEiTQqmY5hrzcEXs6b7heJxeAqU3llVjmPGrXCCP3HUWToDHVvqFWVPdTQZUwFFkKgExwSygAjZLLpVEK4SAumApVdshQuSAJgUeUoPRSlQ7G4ZkuErrtZJGUXNguZhKwFrA8z9F3uEwSTMwQJ2G5+X1USNMa5OjRSwDWRPicedwPIfdHh8W472vEKNqXa7af8A5Bj7EqYpoZmb7c45lYPfZ6MKj0aeysux1N2UuDXNOUWJ8QNutl6Dtpw3PLxYioQCB8Muyx9Vxuw5Artqf+ZnsCJ+pXte2ngqtjTvmuA0BDmEEe6xyOpa8MLts8AeJ1S7K7SAD7LDxQvdBPhaZudAAPE4jX+wu9XwVakS+tTohn6ZLjqRlBJg/KyxDhtV7P8AU1IZR+FsnM4N/BbkTBieS0jkTIydHjWNBJ1jTqnDDAAmbAeqbiRc+ZRYckGeS6WzOMTEaE3HsbJLgQYIhbm3NtJ+6ZVqCIIBHVPkS8aas5wKY1yCo2D9Fu4Bge/xNGidKlRrTGuWZdHoCm+jGndHqOxPCXw7GvAbSptqBhJg1KuUt8A3a3MZPMAc40s4XWrWqQ0ODbvMBzIsTG8RBC9zj6NKt/tua1uEw7R4Bo6AcjWAa6Xv9V8x7QYsueW3Efhv9DGkQI2XG1ykd6XCB5riWGNKo+mSDlMZmmQRsQUgUYEn2TqzpMwguSuyPRxSqxbh6Jbmp9h5/wB0SndEyQc5UUydVEaA1d70hMD+YSGp5boqA9J2P487C1ILj3bjePhP5gNxzG6+p4vCUMazLVYC4CWlpGYA/HRduP0mQvhjHL2HZXtBkinUJyT4XDWmeY/TzH9NIKFdouzL8OfzUyfDUAMHofyu6H5rzlWhC+44fFNqjuqoaXOG96dZux/y8vReI7U9lO6mpSBNPcG7qfR3Nv6vfqCPnb2JRauriMPCxvYgZkyqiE4hCWpAKSnJ72peVIBQCKERUASYAkLu8OlrA3fKSfNwJ+kLl4SkHPaDoTfy1PyXTZUl7rRIKhnRhXp2HU7NbyaPmJP1WLiNYm5Ow0HJdGoYzdBC4WPNh5n+/NZJWdctHpOzTYpNO8l3zXv+1GJYaVKu4BwDWOg7HNM+cErwnBrUmj9I+a7nGqTqtCnTE/gzHkAPqb3XJPcjCDMvAmjiWK/3J7pknJs7QAHm3dL7ecRDqppUz4KQvGmYben1lHwqsMFSc5kd5UBDObZiXeQHzheZ4jZjidSQPuforgk3roUnbo4TnqPqQ081VQhIqGXAcl2BJ0OpCGpeabm/0H7lW9ylJvl+3kFRLXgVQBzeRGh2I3C9n/8AmfDmtxbH1BGSmahJ/O4HIyOgJceoHKF5PD0Gue0SbkSfrZeibjBRqF2YtL2CCDGhMkHmNfcLOb1Q+G+TPqHaTjeHNIGo6S0hwPONQV8S4pxMPLoAEm0bJHGK9V7iXVC9s6zAPm3YrAxEca7M55W9IYQT90tztgrfJ/ZXpqtDIDLzVR5AIiCfPYJrqQGpv9EAZ8itNFHqokFAApoekApjCtBDgU+hVhZmlG0pjPb9m+PgAUq16fwnemebenRe+wnEJhlVwII/261i17Ts/bpPW6+J0KkL1PAOOmmMj/HTOrTt1byP1TEeg7VdlIzVKLbC76Y+H9TObem3lp4DE4eF9Y4bxcZQHOzU/gePxU42cOQ5beS4/ajs4HA1aQE6lrdCD8TB9vZAHzFzEDgunicMQsL2QgDOaZKncrR33RKqVyodjVCKzALDVLDU0BFCXQjRwsQXONobA8yR/KfghNQDmY91nEgCQRBnSNdD5LRwn/vt/wDY+zSfspfR1Y/EdrEGzv8AJcfH0/A0zq9w8oA/ddTGO8PmVzcRfIOb/sFmjpzdHsOAYQ1HMpgxOp5AC59gu/xip3bHvggMpkEDkBosXZ9gp03VXaNb/J+yHtLi82ALokuplxPPMAYHuVwtXI54ajZ5F3Fe8fmcdgANgBsFm4rUlo85+S5rKgIg2O02RU6hgg3iIXZGCTM8buQh9llp8/VOxxAho31SgtipPZHuTmRpfmdvUrK9a6LQQ0Rrc+Q58+iYR2zbgC0S4Do035iTf291o4u/PTaRBIB9P5ssJra9AABsJIIhG2vDTOmv3+6mipPVHM7okZj+Hnz8lKVyir4gu3sNAl0TdUczNLyBoLpBBTqjwOpSTU2CSBj6DI80D3DUo9BPT2SmvpgeIEnkFN2VVICeqip1Zm1P5lRP/BAtTAlg2RNK0IHNRJYKMOTGNYVqo1oWIFE1yYHp+E8XdTNtNwdD/ea9rwziocy0lmpbPjpmdW8x8j0XyqlUXZ4dxBzCCDBG6Yj2PHOBtqDPTgk3EaP5kDZ3Nq8TjcHC9lw3i2YWGv4mbGPiZyP9uFo4pw1tdneU/wAW+0nk7k7rv80AfL61OFnIXdx+Cc0mQRe8i4KxYrAZWsM3cCSLCBPhhJhRiDE/CYYk5phoNyefICUVHDl21tzyWmwsw3FsrjH8FZtm2OFu2IxMgQDI5EGD5E/SU3gF3uP5WHzBJA/dLq0nC7Jad2bH/H9k7hNTwvdF5aPYE/cKG9HQv3RvxIkNAEkm3UrHi6LhUpti4JJHIzC0Yh9h5WQ4RhdiaQJuQCfNzib+ihdDzvR63jrnNwzWNtmbl65nut7BpPoOix8Tq/8ATCmfw5WNA8tR7SncRYauLbSBsC1g5ZnRJ+ax8btmtYVXAXHPL9lzVszXRyqn+nY2XUyfmsOJxTKkGm3KBIPmtrMO2pq8DoVj4hhm0wchnUn2XRGrM4fscLEPl5KIJNO6eFsxRfopyOhUNx0H1/hC5JJINk0JumbKrruA3AI9Exj5ad7SPLceixsxBtImN90feiCADEyOnMIFysUShlE5p/pQOCZA1q0U2jzKxNfCNtUqWmNNGms6brO9GatkslJIbYKikqJiLCMIQiCokMK5VAqwEJjDZdOZTWb0TaFPMYVWNGiwTQ7kjp4TmnMotS5orgNwmMLTqvV8J4xeRAdEH8rxyI5rxdRzQYV0cZlM7KrsmqPoeMZRxAlzdIm8Ecg78w1j67LyvEOFVcRXy0aT3Mb4Q5oJb4ZzQYgwZFuSTWx5fTDhPJx21/4Xe4BjqlHBvqGbkspNPM7DzJlROTSsvHBSdHFrYF1EZYc3eHS0naSPRcrE05EkTGvOE7iXGqr2sZUuGzBIvfX5hcmpi3bFSov00lOK0StVy6FbuGvikCdXPJ9BAv7Lj1DOy6tIeFnQfW6UloMUm5G4EFwHMwE3hxBx1tA4AH/EQsbsU5mYMsXDKTbQ/TZN4E8jEzqM1/zaahZ1qyssrVf09IKn/Ul8/H7Sf+FxuKYgkuJ07xxA2kkn6Qu7hqhD2tFgD3lQamW3hx8gAfNeW4tXMAkQ0nbrN/ssIK2KWkJpYYuvmWfiLcrCJuYCgxnKUjFOLm3BF9+S6UnZjEw0wmoWt2Ca2nzIVtlIWQkVGHNothaOpQPqFt2j3Sv6BrWxbMKTsrrYfL+Ix03QnHVIs6PKEh7ybkyUJS9Jbj4UVHBW1EGyVdkClYK1tw07LNUgaXSuwogKMOCSoCm0FmptMFRIDlFFMdhFECqeFbQqEWHK6dQE+LRA4q20SbpiNjYiZCMtB+ILAaZRspOPwlKq9Lv+GjvXD49NlXfEm5SchmIgo3UiqVC2bGUwU4UoGi5TKhC0jiDoiyGmCaPR8ZaKbaeGZfIAap51CJy+Qk+/RYDiXZgZJI+VoXHbXJOqJ9a0TqZP0H390VfY1KujXxnFd44GIIF40J6eQgeizUKAcDH4tglCr4SCJ0g8kFPMTZJqlSFdu2NZQMwVtqG5Hp7JDKJkHNuDG1lbnXlZ3ZtBUbGxaVv7HZTipMeEVHmdIawu+y5ArQPwgk6Ek2t7dUzgxHen19oScfiwnI7vDuIZ6tRwJJLarp0EZT8rhcXiZktaJgNkjqV1+H4YDvAySSyJ5Bz2/wDC4OPB76plOjiPayzglyJk9B0qjBsQVnx9QGMpOu6p76m4+izuJPutUiEUzX+8kyTukvdFxsgOLdyCqrKtI0FW9ttliNUlCSjiHMfVyACIJ35BZyUTKROgT2YYi6ekQ9lNw7ouFV2p/wDr6gdcj1AKVWxLjy9lC5elfHwQ93VRjBufQaplF7fibPXdbKdCk78LwDyNinKVCUbMYDeSB5GwW+rgoF3CPMJBwgIkVG+RN0lJDcWZFERaqWhBpNMqxSPJa3YtuopmNriVbce4WFJnmblZcpfRdR+zG3DOOy00sEbao3cQIjwD91T+MvOjWhJvI+kNKH2aHUiAZZJjUmL81nFOqbSAEFbirnHSPp7LPUrPfefQIUZe0U5R8s191lO085lERzlDhMTTy+N0OGnhmfZMq41hADdec6+myHyBcShli4ELDiC2fDomV6jo2jzus1Oi46BaQVbZE3ekFSN0TtYR08K5MqYUuuq5qxKDaKo1G3k/st3eUw38Qk7Lj1GFpgiEzCUw50Ex90mk9iTa0dQNsXTI29VlcU+qMoi8bSszlCNl0NDd+QKVgHHvQW+3ROa6GH2WIgiHAkGTcWKZOTw9rwyraq05Z7sOInxZQ4GYXlH4oiq92uZ5+q3dn2RUJn8TKjDP6m8z1AWQ4WCD+sfVZqlJiptBO4ltlCz164cNIukOpuJMCeahpFovzC0SROxbwlwmQhVADCjgihRyAG4XEBoIcDHREcY3ZpjzWUhOp4bcqXGPbC30gatZp2SgpUNzCtgVJUiXsYwKnFGxhO6qphiNLpDFPVAo3MO9kuFSEEooqQBszNiBsmEbqKKWNAVWzqYSMkbzKiiEADigUUVkstqohRRADqFGV0GtgK1FlJ2aLRpweGc8w36wumzgjwJzA+/3UUWb6NodnG428AZD+LN7QsWCweeTPl5q1E18MeiX88mzbjqTm5WudNp8gscq1FWN3FMqWnQT3WjYKqTJb6qKJy6IfZvoOjRWfhP6gfmqUWAwmsFwBAkyT9gFn4ywDLH9sFFE4/sgfRzHIZUUXSZkVOUUQBTDBHmjr15toFFEUSDUw5AB2WrC4TMJaVFFnOTUbLjFXRrZg3bonYMn/lRRc/5ZG344mSthSsNSmQVFF0Y5NmU4pFKKKLUzP//Z' }} />
                                            </Body>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Row>

                                                    <Button icon rounded style={s.softButton}>
                                                        <Icon style={s.softIcon} name="mail" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-linkedin" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-twitter" />
                                                    </Button>

                                                </Row>
                                            </Body>
                                        </CardItem>
                                    </Card>

                                    {/* Tarjeta Adriana */}
                                    <Card style={{ flex: 0 }} style={[s.ml1, s.mr2, s.mt1]}>
                                        <CardItem style={[s.bordered]}>
                                            <Left>
                                                <Body>
                                                    <Text style={[s.title]}>Adriana Beltrán</Text>
                                                    <Text note>Programadora</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Image style={[s.avatar]} source={{ uri: 'http://www.uncanny.ch/wp-content/uploads/2014/12/the_hobbit_battle_of_the_five_armies_tauriel.jpg' }} />
                                            </Body>
                                        </CardItem>
                                        <CardItem>
                                            <Body style={s.center}>
                                                <Row>

                                                    <Button icon rounded style={s.softButton}>
                                                        <Icon style={s.softIcon} name="mail" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-linkedin" />
                                                    </Button>

                                                    <Button icon style={s.softButton}>
                                                        <Icon style={s.softIcon} name="logo-twitter" />
                                                    </Button>

                                                </Row>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </ScrollView>

                                <H1 style={[s.content, s.title, s.thin, s.mt2]}>Sugerencias</H1>
                                <Text style={[s.content, s.subtitle, s.thin]}>En SIABUC sabemos que siempre hay algo que se puede mejorar. Si tienes alguna sugerencia (o deseas reportar algún problema) te lo agredecíamos enormemente. </Text>

                                <View style={[s.center, s.mt1, s.mb2, s.content]}>
                                    <Button iconLeft info>
                                        <Icon name='text' />
                                        <Text>¡Dar sugerencia!</Text>
                                    </Button>
                                </View>

                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        );
    }
}

export default About;