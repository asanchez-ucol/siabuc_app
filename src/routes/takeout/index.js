import React, { Component } from "react";
import { AsyncStorage, View, StyleSheet } from 'react-native';
import { Container, Content, Text, Header, Left, Button, Icon, Thumbnail, Title, Body, Right } from 'native-base';

import TakeoutCard from '../../components/TakeoutCard';
import Logo from '../../components/Logo';


class TakeoutScreen extends Component {
  constructor(props) {
    super(props);
    this.getTakeoutList = this._getTakeoutList.bind(this);
    this.state = {
      takeoutList: null,
      listReady: false,
      items: 0
    }
  }

  async _getTakeoutList() {
    // Se debería poner un setState para establecer el listReady a false mientras se actualiza la lista?
    let takeoutList = await AsyncStorage.getItem('localTakeout');
    let localTakeout = await JSON.parse(takeoutList) || [];
    this.setState({
      items: localTakeout.length,
      localTakeout,
      listReady: true
    });
  }

  componentWillMount() {
    //this.getTakeoutList();
  }

  renderHead() {
    if (this.state.listReady && (this.state.localTakeout && this.state.localTakeout.length > 0)) {
      var head = <View style={{ alignItems: 'center', backgroundColor: 'white' }}>
        <View style={styles.gap}></View>

        <Thumbnail square style={{ width: 110, height: 110 }} source={require('./bag.png')} />

        {/* Botón para confirmar orden
        < View style={{ alignItems: 'center' }}>
          <Button bordered style={[styles.pt]}>
            <Text>ORDENAR TODOS</Text>
          </Button>
        </View >*/}

        {/* Número de ejemplares que están agregados a la lista */}
        < Text style={[styles.info]} >{this.state.items} ejemplares en mi orden</Text >

        <View style={styles.gapb}></View>
      </View >;

      return head;
    }

  }

  renderItem() {
    if (this.state.listReady && (this.state.localTakeout && this.state.localTakeout.length > 0)) {
      return this.state.localTakeout.map((takeoutCard) => {
        return <TakeoutCard navigation={this.props.navigation} takeoutCard={takeoutCard} key={takeoutCard.id} getTakeoutList={this.getTakeoutList} />;
      });
    }

    return (
      // Sería bueno que esto fuera un componente
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }} >
        <Thumbnail square style={{ width: 120, height: 120, marginTop: "30%" }} source={require('./bag.png')} />
        <Text style={[styles.info, styles.pt]}>Parece que aún no agregas ningún libro a tu orden, intenta buscar algún título que desees llevar contigo y cuando estés listo para agregarlo solo toca un botón como este   </Text>
        <Button style={[styles.center, styles.weirdButton]} bordered>
          <Icon name='ios-basket' />
        </Button>
      </View>
    );
  }

  render() {
    this.getTakeoutList();
    return (
      <Container style={{ backgroundColor: 'white' }}>
        <Header>
          <Left>
            <Logo />
          </Left>
          <Body>
            <Title>Mi lista</Title>
          </Body>
          <Right>
            {/*<Button transparent>
              <Icon name='more' />
            </Button>*/}
          </Right>
        </Header>
        <Content scrollEventThrottle={300} onScroll={this.setCurrentReadOffset} removeClippedSubviews={true} >
          {this.renderHead()}
          {this.renderItem()}
        </Content>
      </Container>
    );
  }
}

export default TakeoutScreen;

const styles = StyleSheet.create({

  upContent: {
    marginTop: '15%',
    marginBottom: '8%',
  },

  info: {
    marginLeft: 35,
    marginRight: 35,
    color: '#546e7a',
    textAlign: 'center'
  },

  m: {
    padding: '2%'
  },

  pt: {
    marginTop: 15,
    marginBottom: 8
  },

  gap: {
    paddingTop: '12%'
  },

  gapb: {
    paddingTop: '4%'
  },

  white: {
    backgroundColor: 'white',
  },

  small: {
    fontSize: 13,
  },

  bigTitle: {
    fontWeight: 'normal',

    fontSize: 52,
    color: '#263238',
    fontFamily: 'sans-serif-light',
  },

  subTitle: {
    color: '#546e7a',
    fontFamily: 'sans-serif-light',
    fontSize: 14
  },

  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  weirdButton: {
    marginLeft: '43%',
  }

});
