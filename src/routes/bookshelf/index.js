import React, { Component } from "react";
import { AsyncStorage, View, StyleSheet, Dimensions } from 'react-native';
import { Container, Content, Text, Header, Left, Button, Icon, Thumbnail, Title, Body, Right } from 'native-base';

import LibreroCard from '../../components/LibreroCard';
import Logo from '../../components/Logo';


class LibreroScreen extends Component {
  constructor(props) {
    super(props);
    this.getLibreroList = this._getLibreroList.bind(this);
    this.state = {
      favoritosList: null,
      listReady: false,
      items: 0,
      screen: Dimensions.get('screen')
    }
  }

  async _getLibreroList() {
    // Se debería poner un setState para establecer el listReady a false mientras se actualiza la lista?

    let favoritosList = await AsyncStorage.getItem('localFavoritos');
    let localFavoritos = await JSON.parse(favoritosList) || [];
    this.setState({
      items: localFavoritos.length,
      localFavoritos,
      listReady: true
    });
  }

  componentWillMount() {
    //this.getLibreroList();
  }

  renderHead() {
    if (this.state.listReady && (this.state.localFavoritos && this.state.localFavoritos.length > 0)) {
      var head =

        <View style={{ alignItems: 'center', backgroundColor: 'white' }}>
          <View style={styles.gap}></View>

          <Thumbnail square style={{ width: this.state.screen.width * 0.44, height: this.state.screen.height * 0.14 }} source={require('./bag.png')} />

          {/* Número de ejemplares que están agregados a la lista */}
          <Text style={[styles.info, styles.pt]} >{this.state.items} EJEMPLAR EN MI LIBRERO</Text >

          <View style={styles.gapb}></View>
        </View >;


      return head;
    }
  }

  renderItem() {
    if (this.state.listReady && (this.state.localFavoritos && this.state.localFavoritos.length > 0)) {
      return this.state.localFavoritos.map((libreroCard) => {
        return <LibreroCard navigation={this.props.navigation} libreroCard={libreroCard} key={libreroCard.id} getLibreroList={this.getLibreroList} />;
      });
    }

    return (
      // Sería bueno que esto fuera un componente
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }} >
        <Thumbnail square style={{ width: this.state.screen.width * 0.44, height: this.state.screen.height * 0.14, marginTop: '30%' }} source={require('./bag.png')} />

        <Text style={[styles.info, styles.pt]}>Parece que aún no agregas nada al librero. Aquí puedes guardar tus libros favoritos y revisar sus detalles o disponibilidad. </Text>
        <Text style={[styles.info, styles.pt]}>Para agregar un libro a esta sección basta con tocar un botón como este </Text>

        <Button style={[styles.center, styles.bookButton]} iconRight bordered>
          <Text>Agregar a librero</Text>
          <Icon name='ios-bookmarks' />
        </Button>
      </View>
    );
  }

  render() {

    this.getLibreroList();
    return (
      <Container style={{ backgroundColor: 'white' }}>
        <Header>
          <Left>
            <Logo />
          </Left>
          <Body>
            <Title>Mi librero</Title>
          </Body>
          <Right>
            {/* aqui andaba un boton*/}
          </Right>
        </Header>
        <Content scrollEventThrottle={300} onScroll={this.setCurrentReadOffset} removeClippedSubviews={true} >
          {this.renderHead()}
          {this.renderItem()}
        </Content>
      </Container>
    );
  }
}

export default LibreroScreen;

const styles = StyleSheet.create({

  upContent: {
    marginTop: '15%',
    marginBottom: '8%',
  },

  info: {
    marginLeft: 35,
    marginRight: 35,
    color: '#546e7a',
    textAlign: 'center'
  },

  m: {
    padding: '2%'
  },

  pt: {
    marginTop: 15,
    marginBottom: 8
  },

  gap: {
    paddingTop: '12%'
  },

  gapb: {
    paddingTop: '4%'
  },

  white: {
    backgroundColor: 'white',
  },

  small: {
    fontSize: 13,
  },

  bigTitle: {
    fontWeight: 'normal',

    fontSize: 52,
    color: '#263238',
    fontFamily: 'sans-serif-light',
  },

  subTitle: {
    color: '#546e7a',
    fontFamily: 'sans-serif-light',
    fontSize: 14
  },

  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  weirdButton: {
    marginLeft: '28%',
  },

  bookButton: {
    marginLeft: '25%'
  }

});
