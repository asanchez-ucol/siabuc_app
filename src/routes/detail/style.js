export default {
    detailsContainer: {
        flex:2,
        flexDirection:'column',
        alignItems:'flex-start',
        marginLeft: 25,
        marginRight: 25,
        marginTop: 20,
    },
    titulo:{
        color:"#212121"
    },
    data:{
        color:"#212121"
    },

    cardData: {
        color:"#616161",
        marginLeft: 10
    },
    //container style wrapper for scrollview
    footerWrapper: {
        flexWrap: 'wrap', 
        alignItems: 'flex-start',
        flexDirection:'row',
    },
    //non-container style wrapper for scrollview
    footerWrapperNC: {
        flexDirection:'column',
    },

    sectionTitle: {
        marginTop: 30,
        marginBottom:10,
        color: '#424242'
    }
    ,
    autor:{
 
        marginTop: 10,
        marginBottom: 10,
        
        color:"#757575",
    },
    button:{
        marginTop:20,
        paddingLeft:5,
        paddingRight:5
    },
    buttonText:{
        fontSize:18,
        color:"white"
    },
    imgContainer:{
        flex:1,
        alignItems:'center',
        backgroundColor:"grey",
    },
    disp:{
        fontSize:20,
        color:"black",
        marginTop:30
    },
    biblioteca:{
        marginTop: 5,
        fontSize:18
    },
    agregarLibrero:{
        justifyContent: 'center',
        marginTop: 15
    },
    img:{
        width: '100%',
        backgroundColor:'black',
        justifyContent: 'center',
        alignItems: 'center'
    }
};
