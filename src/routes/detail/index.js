import React, { Component } from "react";
import { View, AsyncStorage, ActivityIndicator, Image, ScrollView, ListView, TouchableOpacity, Dimensions, StyleSheet } from 'react-native';
import Style from './style.js';
import { Icon, Button, Badge, Card, CardItem, Text, H1, H2, H3, Toast, Thumbnail, Header, Left, Body, Title, Right, Container } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Portada from './../../components/Portada';
import Chip from './../../components/Chip';

class DetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: false,
      isLoading: true,
      dataSource: null,
      dataSourceEjemplares: null,
      imgHeight: 200,
      resize: true
    }
  }

  componentDidMount() {
    //Obtener los datos del libro
    return fetch('http://siabuc.ucol.mx:3000/api/ficha/' + this.props.navigation.state.params.id)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function () {
          if (responseJson.exists == true) {
            this.setState({ item: true });
          }
        });
      })
      .catch((error) => {
        Toast.show({ text: 'Error al conectar al servidor', position: 'bottom', duration: 3000 });
      });
  }

  async guardarLibro() {
    let librero;
    try {
      librero = await AsyncStorage.getItem('localFavoritos');
      librero = JSON.parse(librero);
      let id = this.state.dataSource.ficha.id;

      if (librero != null) {
        let found = librero.find(function (element) {
          return element.id == id;
        });
        if (found) {
          Toast.show({ text: 'El libro ya está en mi librero', position: 'bottom', duration: 3000 });
        }
        else {
          librero.push(this.state.dataSource.ficha);
          AsyncStorage.setItem('localFavoritos', JSON.stringify(librero));
          Toast.show({ text: 'Nuevo libro agregado a mi librero', position: 'bottom', duration: 3000 });
        }
      } else {
        let aux = [];
        aux.push(this.state.dataSource.ficha);
        AsyncStorage.setItem('localFavoritos', JSON.stringify(aux));
        Toast.show({ text: 'El libro se ha agregado a mi librero', position: 'bottom', duration: 3000 });
      }
    } catch (err) {
      Toast.show({ text: 'Error al conectar al servidor', position: 'bottom', duration: 3000 });
    }
  }

  async agregarCarrito(bibliotecaId, bibliotecaN) {
    let takeout;
    try {
      takeout = await AsyncStorage.getItem('localTakeout');
      takeout = JSON.parse(takeout);
      let id = this.state.dataSource.ficha.id;

      if (takeout != null) {
        let found = takeout.find(function (element) {
          return element.id == id;
        });
        if (found) {
          let found2 = takeout.find(function (element) {
            return element.idBiblioteca == bibliotecaId;
          });
          if (found2) {
            Toast.show({ text: 'El libro ya se ha añadido a mi lista', position: 'bottom', duration: 3000 });
          }
          else {
            takeout.push({
              id: this.state.dataSource.ficha.id,
              titulo: this.state.dataSource.ficha.titulo,
              isbn: this.state.dataSource.ficha.isbn,
              clasificacion: this.state.dataSource.ficha.clasificacion,
              autor: this.state.dataSource.ficha.autor,
              idBiblioteca: bibliotecaId,
              nBiblioteca: bibliotecaN,
              portada: this.state.dataSource.ficha.portada
            });
            AsyncStorage.setItem('localTakeout', JSON.stringify(takeout));
            Toast.show({ text: 'Nuevo libro agregado a mi lista', position: 'bottom', duration: 3000 });
          }
        }
        else {
          takeout.push({
            id: this.state.dataSource.ficha.id,
            titulo: this.state.dataSource.ficha.titulo,
            isbn: this.state.dataSource.ficha.isbn,
            clasificacion: this.state.dataSource.ficha.clasificacion,
            autor: this.state.dataSource.ficha.autor,
            idBiblioteca: bibliotecaId,
            nBiblioteca: bibliotecaN,
            portada: this.state.dataSource.ficha.portada
          });
          AsyncStorage.setItem('localTakeout', JSON.stringify(takeout));
          Toast.show({ text: 'Nuevo libro agregado a mi lista', position: 'bottom', duration: 3000 });
        }
      } else {
        let aux = [{
          id: this.state.dataSource.ficha.id,
          titulo: this.state.dataSource.ficha.titulo,
          isbn: this.state.dataSource.ficha.isbn,
          clasificacion: this.state.dataSource.ficha.clasificacion,
          autor: this.state.dataSource.ficha.autor,
          idBiblioteca: bibliotecaId,
          nBiblioteca: bibliotecaN,
          portada: this.state.dataSource.ficha.portada
        }];
        AsyncStorage.setItem('localTakeout', JSON.stringify(aux));
        Toast.show({ text: 'El libro se ha agregado a mi lista', position: 'bottom', duration: 100 });
      }
    } catch (err) {
      Toast.show({ text: 'Error al conectar al servidor', position: 'bottom', duration: 100 });
    }
  }

  getIcon(tipo) {
    let icon;
    switch (tipo) {
      case 1:
        icon = 'book';
        break;
      case 2:
        icon = 'ios-bookmarks';
        break;
      case 3:
        icon = 'ios-map';
        break;
      case 4:
        icon = 'ios-videocam';
        break;
      case 5:
        icon = 'disc';
        break;
      case 6:
        icon = 'image';
        break;
      case 7:
        icon = 'ios-photos';
        break;
      case 8:
        icon = 'bookmark';
        break;
      case 9:
        icon = 'ios-musical-notes';
        break;
      case 10:
        icon = 'ios-film';
        break;
      case 11:
        icon = 'document';
        break;
      case 12:
        icon = 'easel';
        break;
      case 13:
        icon = 'ios-body';
        break;
      case 14:
        icon = 'ios-more';
        break;
      case 15:
        icon = 'ios-refresh';
        break;
      case 16:
        icon = 'ios-stats';
        break;
      case 17:
        icon = 'ios-checkmark';
        break;
      case 18:
        icon = 'ios-book';
        break;
      case 19:
        icon = 'md-bookmarks';
        break;
    }
    return icon
  }

  changeSize() {
    if (this.state.resize) {
      this.setState({
        resize: false
      });
    }
    else {
      this.setState({
        resize: true
      });
    }
  }

  render() {
    const { goBack } = this.props.navigation;
    const screen = Dimensions.get('screen')
    const { dataSource } = this.state;

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 20, backgroundColor: 'white' }}>
          <ActivityIndicator />
        </View>
      );
    }
    else if (this.state.item) {

      if (this.state.resize) {
        return (
          <Container>
            <Header style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.5)', position: 'absolute', zIndex: 99999999999 }}>
              <Left>
                <Button iconLeft bordered light style={{ borderColor: 'transparent' }} onPress={() => goBack()}>
                  <Icon name='arrow-back' />
                  <Text>Regresar</Text>
                </Button>
              </Left>
              <Body></Body>
            </Header>
            <ScrollView style={{ backgroundColor: 'white' }}>

              <View style={{ flex: 1, marginBottom: 10 }}>
                <View style={Style.imgContainer} style={{ flexDirection: 'column' }}>

                  <TouchableOpacity style={[Style.img, { height: this.state.imgHeight }]} resizeMode={"cover"} onPress={() => { this.changeSize() }}>
                    <Portada
                      portada={(this.state.dataSource.ficha.portada === '' ? 'empty' : this.state.dataSource.ficha.portada)}
                      isbn={(!this.state.dataSource.ficha.isbn || this.state.dataSource.ficha.isbn === '[sin ISBN]' || this.state.dataSource.ficha.isbn === null || this.state.dataSource.ficha.isbn === 'NULL' ? '0000' : this.state.dataSource.ficha.isbn.replace(/-/g, ''))}
                      onPress={() => { this.changeSize() }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={Style.detailsContainer}>
                  <H2 style={Style.titulo}>{dataSource.ficha.titulo} </H2>
                  <H3 style={Style.autor}>{(dataSource.ficha.autor === '' ? '[sin autor]' : dataSource.ficha.autor)}</H3>

                  <Button iconRight bordered onPress={() => { this.guardarLibro() }}>
                    <Text> AGREGAR A LIBRERO </Text>
                    <Icon name='ios-bookmarks' />
                  </Button>

                  <H3 style={Style.sectionTitle}>Características</H3>
                  <Grid>
                    <Col style={{ flex: 1 }}>
                      <Text style={{ color: 'grey' }}>Editorial</Text>
                      <Text style={Style.data}>{dataSource.ficha.editorial} en {dataSource.ficha.fecha}</Text>
                    </Col>
                    <Col style={{ flex: 1 }}>
                      <Text style={{ color: 'grey' }}>Clasificación</Text>
                      <Text style={Style.data}>{dataSource.ficha.clasificacion}</Text>
                    </Col>
                  </Grid>


                  <H3 style={Style.sectionTitle}>Temas</H3>
                  <ScrollView
                    horizontal={false}
                    style={Style.footerWrapperNC}
                    contentContainerStyle={[Style.footerWrapper]}>
                    {this.state.dataSource.ficha.temas.map(record => {
                      return (
                        <Chip key={Math.random()} content={record} />
                      );
                    })}
                  </ScrollView>

                  <H3 style={Style.sectionTitle}>Disponible en</H3>

                  {this.state.dataSource.ejemplares.map(record => {
                    return (
                      <Card style={{ paddingLeft: 0, width: '98%' }} key={record.id}>
                        <CardItem>
                          <Grid>
                            <Row style={{ flex: 1 }}>

                              <Col style={{
                                width: '10%',
                                borderRightWidth: 1,
                                borderColor: '#d6d7da',
                              }}>
                                <Icon name={this.getIcon(record.tipo)} style={{ color: "grey" }} />
                              </Col>

                              <Col style={{ width: '78%' }}>
                                <Text style={Style.cardData}>{record.biblioteca}</Text>
                              </Col>

                              <Col style={{ width: '12%', marginLeft: '4%' }}>
                                <Icon name='ios-basket' style={{ color: '#E12726' }} onPress={() => { this.agregarCarrito(record.id, record.biblioteca) }} />
                              </Col>

                            </Row>
                          </Grid>
                        </CardItem>
                      </Card>
                    );
                  })}
                </View>
              </View>
            </ScrollView>
          </Container>
        );
      }
      return (
        <TouchableOpacity onPress={() => { this.changeSize() }}>
          <Portada
            portada={(this.state.dataSource.ficha.portada === '' ? 'empty' : this.state.dataSource.ficha.portada)}
            isbn={(!this.state.dataSource.ficha.isbn || this.state.dataSource.ficha.isbn === '[sin ISBN]' || this.state.dataSource.ficha.isbn === null || this.state.dataSource.ficha.isbn === 'NULL' ? '0000' : this.state.dataSource.ficha.isbn.replace(/-/g, ''))}
            onPress={() => { this.changeSize() }}
          />
        </TouchableOpacity>
      );
    }
    return (
      /* Vista que se muestra cuando hay algún error */
      <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', backgroundColor: 'white' }} >
        <View style={{ flex: 1 }} />
        <View style={{ flex: 4, alignItems: 'flex-start', justifyContent: 'center' }}  >
          <Thumbnail style={{ width: screen.width * 0.62, height: screen.height * 0.29 }} square source={require('../../../assets/images/error.jpg')} />
        </View>
        <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}  >
          <H3 style={styles.info}>Ha ocurrido un error al tratar de mostrar este libro</H3>
        </View>

        <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', marginTop: 14 }}  >
          <Text style={styles.info}>Intenta abrir otro libro que tenga un título similar o inicia una nueva búsqueda con otras palabras</Text>
        </View>

        <View style={{ flex: 2, alignItems: 'flex-start', justifyContent: 'center' }}  >
          <Button iconLeft style={{ marginLeft: 35 }} onPress={() => goBack()}>
            <Icon name='arrow-back' />
            <Text>Regresar</Text>
          </Button>
        </View>

        <View style={{ flex: 1 }} ></View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  info: {
    marginLeft: 35,
    marginRight: 35,
    color: '#424242',
    textAlign: 'left'
  },
  secondaryInfo: {
    marginLeft: 35,
    marginRight: 35,
    color: '#757575',
    textAlign: 'left'
  },
});

export default DetailScreen;
