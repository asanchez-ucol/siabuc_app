import React, { Component } from 'react';
import { View, CheckBox } from 'react-native';
import { StyleProvider, getTheme, Container, Header, Left, Content, Icon, Button, Input, Picker, Item, Body, Text, Right } from 'native-base';
import Panel from '../../components/Panel';

class FilterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tipoMaterial: 'key1',
      listaTiposMaterial: null,
      tiposPopulated: false,
      listaBibliotecas: null,
      bibliotecasPopulated: false,
      filtersTitulo: null,
      filtersAutor: null,
      filtersTema: null,
      filtersIsbn: null,
      filtersEditorial: null,
      filtersSerie: null,
      filtersClasificacion: null,
      filtersNotas: null,
      filtersTiposMaterial: null,
      filtersBibliotecas: null
    }
    this.filtersConfigured = this.props.navigation.state.params.filtersConfigured;
    this.listaTipos = null;
    this.listaBibliotecas = null;
  }

  getListaBibliotecas() {
    fetch ('http://siabuc.ucol.mx:3000/api/bibliotecas')
      .then (response => response.json())
      .then (responseJson => this.setState({
        listaBibliotecas: responseJson,
        bibliotecasPopulated: true,
      }))
      .catch((error) => {
        console.error(error);
      })
  }

  getTiposMaterial() {
    fetch ('http://siabuc.ucol.mx:3000/api/material/tipos')
      .then (response => response.json())
      .then (responseJson => this.setState({
        listaTiposMaterial: responseJson.data,
        tiposPopulated: true,
      }))
      .catch((error) => {
        console.error(error);
      })
  }

  componentWillMount () {
    this.getTiposMaterial();
    this.getListaBibliotecas();
  }

  setNewKeyInArray(newKey, originalArray) {
    let arrayValores = originalArray || [];
    let indexNV = arrayValores.indexOf(newKey);
    if (indexNV < 0) {
      arrayValores.splice(arrayValores.length, 0, newKey);
    } else {
      arrayValores.splice(indexNV, 1);
    }
    return arrayValores;
  }

  findKeyInArray(key, originalArray) {
    let arrayValores = originalArray || [];
    if (arrayValores.indexOf(key) < 0) {
      return false;
    } else {
      return true;
    }
  }

  renderListaTiposMaterial() {
    if (this.state.tiposPopulated) {
      return this.state.listaTiposMaterial.map(item => {
        return <View key={ item.key } style={{ flexDirection: 'row' }}>
          <CheckBox
            value={this.findKeyInArray(item.key, this.state.filtersTiposMaterial)}
            onValueChange={() => this.setState({filtersTiposMaterial: this.setNewKeyInArray(item.key, this.state.filtersTiposMaterial)})} />
          <Text style={{marginTop: 5}}> { item.material }</Text>
        </View>;
      });
    }
  }

  renderListaBibliotecas() {
    if (this.state.bibliotecasPopulated) {
      return this.state.listaBibliotecas.map(item => {
        return <View key={ item.id } style={{ flexDirection: 'row' }}>
          <CheckBox
            value={this.findKeyInArray(item.id, this.state.filtersBibliotecas)}
            onValueChange={() => this.setState({filtersBibliotecas: this.setNewKeyInArray(item.id, this.state.filtersBibliotecas)})} />
          <Text style={{marginTop: 5}}> { item.biblioteca }</Text>
        </View>;
      });
    }
  }

  evaluaFiltroCadena(nombre, filtro) {
    return filtro ? `&${nombre}=${filtro}` : '';
  }

  evaluaFiltroArray(nombre, filtro){
    let filtroArray = filtro || [];
    return filtroArray.length < 1 ? '' : `&${nombre}=${filtro.toString()}`
  }

  configureFilters() {
    let filterString = this.evaluaFiltroCadena('titulo', this.state.filtersTitulo) +
      this.evaluaFiltroCadena('autor', this.state.filtersAutor) +
      this.evaluaFiltroCadena('tema', this.state.filtersTema) +
      this.evaluaFiltroCadena('isbn', this.state.filtersIsbn) +
      this.evaluaFiltroCadena('editorial', this.state.filtersEditorial) +
      this.evaluaFiltroCadena('serie', this.state.filtersSerie) +
      this.evaluaFiltroCadena('clasificacion', this.state.filtersClasificacion) +
      this.evaluaFiltroCadena('notas', this.state.filtersNotas) +
      this.evaluaFiltroArray('material', this.state.filtersTiposMaterial) +
      this.evaluaFiltroArray('biblioteca', this.state.filtersBibliotecas);
    this.filtersConfigured(filterString === '' ? null : filterString);
    this.props.navigation.goBack();
  }

  render() {
    return (
      <Container>
        <Header>
          <Right>
            <Button info iconLeft onPress={() => this.configureFilters()}>
              <StyleProvider style={getTheme({ iconFamily: 'MaterialIcons' })}>
                <Icon name='done-all' />
              </StyleProvider>
              <Text>Aplicar Filtros</Text>
            </Button>
          </Right>
        </Header>
        <Content>
          <Item>
            <StyleProvider style={getTheme({ iconFamily: 'MaterialIcons' })}>
              <Icon name='title' />
            </StyleProvider>
            <Input placeholder='Título' onChangeText={text => this.setState({ filtersTitulo: text })}/>
          </Item>
          <Item>
            <StyleProvider style={getTheme({ iconFamily: 'MaterialIcons' })}>
              <Icon name='person' />
            </StyleProvider>
            <Input placeholder='Autor' onChangeText={text => this.setState({ filtersAutor: text })}/>
          </Item>
          <Item>
            <StyleProvider style={getTheme({ iconFamily: 'MaterialIcons' })}>
              <Icon name='subject' />
            </StyleProvider>
            <Input placeholder='Tema' onChangeText={text => this.setState({ filtersTema: text })}/>
          </Item>
          <Item>
            <StyleProvider style={getTheme({ iconFamily: 'FontAwesome' })}>
              <Icon name='barcode' />
            </StyleProvider>
            <Input placeholder='ISBN' onChangeText={text => this.setState({ filtersIsbn: text })}/>
          </Item>
          <Item>
            <StyleProvider style={getTheme({ iconFamily: 'FontAwesome' })}>
              <Icon name='edit' />
            </StyleProvider>
            <Input placeholder='Editorial' onChangeText={text => this.setState({ filtersEditorial: text })}/>
          </Item>
          <Item>
            <StyleProvider style={getTheme({ iconFamily: 'MaterialCommunityIcons' })}>
              <Icon name='library' />
            </StyleProvider>
            <Input placeholder='Serie' onChangeText={text => this.setState({ filtersSerie: text })}/>
          </Item>
          <Item>
            <StyleProvider style={getTheme({ iconFamily: 'FontAwesome' })}>
              <Icon name='hashtag' />
            </StyleProvider>
            <Input placeholder='Clasificación' onChangeText={text => this.setState({ filtersClasificacion: text })}/>
          </Item>
          <Item>
            <StyleProvider style={getTheme({ iconFamily: 'Foundation' })}>
              <Icon name='clipboard-notes' />
            </StyleProvider>
            <Input placeholder='Notas' onChangeText={text => this.setState({ filtersNotas: text })}/>
          </Item>
          <Panel style={{flex: 1, backgroundColor: '#f4f7f9', paddingTop: 30}} name='location' family='Entypo' title='Biblioteca'>
            { this.renderListaBibliotecas() }
          </Panel>
          <Panel style={{flex: 1, backgroundColor: '#f4f7f9', paddingTop: 30}} name='tag-multiple' family='MaterialCommunityIcons' title='Tipo de Material'>
            { this.renderListaTiposMaterial() }
          </Panel>
        </Content>
      </Container>
    );
  }
}

export default FilterScreen;
