import React from 'react';
import { AdMobBanner } from 'react-native-admob';

export function getBanner() {
  return <AdMobBanner
    key={Math.random()}
    adSize="smartBannerPortrait"
    adUnitID="ca-app-pub-5092109841439259/9439682380"
    onAdFailedToLoad={error => console.error(error)} />;
}

export function fetchData(callback, targetUri) {
  fetch(targetUri)
    .then(response => response.json())
    .then(callback)
    .catch(error => {
      console.error(error);
    });
}