export function setDefaultDictionary() {
    lang = AsyncStorage.getItem('config.lang') || 'es_Mx';
}

export function loadDictionary() {
    global.defaultLanguage = require(`./${global.defaultLanguage}.json`);
}

export function getScreen(dictionary, screen, ) {
    return dictionary[screen] ? dictionary[screen] || {} : null;
}
