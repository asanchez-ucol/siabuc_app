import React, { Component } from "react";
import { Icon } from "native-base"
import { StackNavigator, TabNavigator, TabBarBottom } from "react-navigation";

import BookShelfScreen from "./routes/bookshelf/";
import DetailScreen from "./routes/detail/";
import SearchScreen from "./routes/search/";
import FilterScreen from "./routes/filter/";
import TakeoutScreen from "./routes/takeout/";
import SplashScreen from "./routes/splash/";

const ShelfNavigator = StackNavigator({
  Shelf: { screen: BookShelfScreen }
},
  {
    initialRouteName: 'Shelf',
    navigationOptions: {
      header: null
    }
  }
);

const SearchNavigator = StackNavigator({
  Search: { screen: SearchScreen },
  Filters: { screen: FilterScreen }
},
  {
    initialRouteName: 'Search',
    navigationOptions: {
      header: null
    }
  }
);

const TakeoutNavigator = StackNavigator({
  Takeout: { screen: TakeoutScreen }
},
  {
    initialRouteName: 'Takeout',
    navigationOptions: {
      header: null
    }
  }
);

const TabStack = TabNavigator(
  {
    Librero: { screen: ShelfNavigator },
    Buscar: { screen: SearchNavigator },
    Lista: { screen: TakeoutNavigator }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: () => {
        const { routeName } = navigation.state;
        let label;

        if (routeName === 'Librero') {
          label = 'Mi Librero';
        } else if (routeName === 'Buscar') {
          label = 'Búsqueda';
        } else if (routeName === 'Lista') {
          label = 'Para Préstamo';
        }

        return label;
      },
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;

        if (routeName === 'Librero') {
          iconName = `ios-bookmarks${focused ? '' : '-outline'}`;
        } else if (routeName === 'Buscar') {
          iconName = `ios-search${focused ? '' : '-outline'}`;
        } else if (routeName === 'Lista') {
          iconName = `ios-basket${focused ? '' : '-outline'}`;
        }

        return <Icon name={iconName} size={25} color={tintColor} />;
      },
    }),
    initialRouteName: 'Buscar',
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      activeTintColor: '#212121',
      inactiveTintColor: '#9e9e9e',
      allowFontScaling: false,
      activeBackgroundColor: '#eceff1',
      inactiveBackgroundColor: 'white',
      showLabel: true,
    },
    animationEnabled: true,
    swipeEnabled: true,
  }
);

const Root = StackNavigator(
  {
    Home: { screen: TabStack },
    Splash: { screen: SplashScreen },
    Details: { screen: DetailScreen }
  },
  {
    mode: 'modal',
    headerMode: 'none',
    initialRouteName: 'Splash'
  }
);

class App extends Component {
  render() {
    return (<Root />);
  }
}

export default App;
